# reGOSH Arg - Proyectos

Lista de proyectos asociados a nodos de reGOSH en Argentina, y links relevantes.

Los proyectos de la red entera están listados en el sitio de proyectos de reGOSH: https://regosh.libres.cc/proyectos/

# Mza

* Cámara de respiración de suelo https://gitlab.com/nanocastro/camara-respiracion-suelo
* Colorimetro reGOSH https://gitlab.com/nanocastro/ColorLabFD
* Laboratorio Agroecologico Abierto https://gitlab.com/nanocastro/labagroecologicoabierto
* Microscopio BrewerMicro: https://gitlab.com/nanocastro/microbrew

# BA

* Sensor de CO2 de Boid: https://gitlab.com/boid-ba/boid_co2
* Robot pipeteador: https://github.com/naikymen/pipettin-grbl-alpha
